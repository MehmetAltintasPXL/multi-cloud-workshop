# Lab 01 - Our application #

The goal of this lab is to run an application on our management server. To make our lives a little bit easier we will be using Docker for this. For people not familiar with Docker: Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and ship it all out as one package.

## 0. Fork the application repository ##

Before doing anything else, open your browser en visit the following GitHub page:

Browse to the workshop-app GitLab page:

- URL: https://gitlab.com/gluo-workshop/multi-cloud-app

On the top right you should see a `Fork` button, click this buttion and fork the repository into your previously assigned GitLab group.

![](../images/forkAppRepo.png)

NOTE: it is important that from now on you only use your own fork.

## 2. Clone repository ##

Download the application code onto your management server by cloning the GitHub repository:

```
cd ~
git clone git@gitlab.com:gluo-workshops/multicloud-workshop/YOUR_GROUP_NAME/multi-cloud-app.git

---

Cloning into 'multi-cloud-app'... 
remote: Enumerating objects: 442, done.
remote: Counting objects: 100% (295/295), done.
remote: Compressing objects: 100% (127/127), done.
remote: Total 442 (delta 146), reused 295 (delta 146), pack-reused 147
Receiving objects: 100% (442/442), 2.47 MiB | 5.49 MiB/s, done.
Resolving deltas: 100% (192/192), done.
```

## 3. Dockerfile ##

We will be using a Dockerfile to create a Docker image, the Dockerfile that we will be using is extremely simple:

```
FROM php:apache

MAINTAINER steven@gluo.be

COPY . /var/www/html/
```

The Dockerfile will do the following:

* start from the `php:apache` base image (this images has Apache & PHP installed and configured)
* set a label for the maintainer (optional)
* copy the application code into the `/var/www/html/` directory of the image

## 4. Build the Docker image ##

Change into the correct directory:

```
cd multi-cloud-app
```

Building the Docker image is as easy as running the command below, make sure to replace `YOURDOCKERHUBACCOUNTNAME` with your actual Docker Hub account name:

```
docker image build -t YOURDOCKERHUBACCOUNTNAME/multi-cloud-app:v1 .
```

Your output should look like:

```
[+] Building 14.9s (7/7) FINISHED                                                                                                                                                              
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 111B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/php:apache
 => [internal] load build context
 => => transferring context: 237.15kB
 => [1/2] FROM docker.io/library/php:apache@sha256:7bfb3a43e0f859f4ccd58489bf0c6eeb1db778c0a79469d21ff4fc8e1802da51
 => => resolve docker.io/library/php:apache@sha256:7bfb3a43e0f859f4ccd58489bf0c6eeb1db778c0a79469d21ff4fc8e1802da51 
 => => sha256:7bfb3a43e0f859f4ccd58489bf0c6eeb1db778c0a79469d21ff4fc8e1802da51 1.86kB / 1.86kB 
 => => sha256:5ca9fb408faadb7bf25d58bd66104b2e93e7ba37e961bb24b245abcc09230187 91.64MB / 91.64MB 
 => => sha256:85ac5bcb0cfa6bb841f62cfcc5b13976d36a38a44814f21b11847dda12863ce2 3.04kB / 3.04kB 
 => => sha256:ee0a4e40ccac1c58f6ca3cf6c0cbb0a5897abd33dee49b641509d5627df02b46 226B / 226B 
 => => sha256:6fe94f625cf534e7c289236ee8f19ac1cc3b442898121595e6642558ca6f0615 12.63kB / 12.63kB  
 => => sha256:f1f26f5702560b7e591bef5c4d840f76a232bf13fd5aefc4e22077a1ae4440c7 31.41MB / 31.41MB
 => => sha256:25862b797d8786d6655c181772787e4fa6c932aafd6853e429409590e9f6d99b 270B / 270B  
 => => sha256:5c128100b25a1cf17b571b5c0c8d316f25c03b14b0315f97302a72713953847a 19.25MB / 19.25MB 
 => => sha256:c471030ba37f74280181f8e9174e2e45192728be49c4023d12ea165263852174 471B / 471B
 => => sha256:285a5ec09eb5e2d43e7ad5c490ed118f537ed46d9be12e573a5fa8aa24632f51 510B / 510B 
 => => extracting sha256:f1f26f5702560b7e591bef5c4d840f76a232bf13fd5aefc4e22077a1ae4440c7     
 => => sha256:aae08d904de274c3439581ae207ac32759146fce216b6744dd1e9dad7bff40e7 12.33MB / 12.33MB 
 => => sha256:b25acd918410cd04cf6d18be60764dd927d398f92f46a57d91e2f5ab7b022002 491B / 491B
 => => sha256:3e0ec17247cba6f489a3591694cd88fffa3dcc7787f34c0164dec799980edb07 11.34MB / 11.34MB  
 => => sha256:53d5f7669583566f165bfceb2ed3e0b8435b7c32b27b9d7c8bf847126ccba9d0 2.46kB / 2.46kB
 => => sha256:d778c6e5f84cb3078c07f5fac6b8580c70a60a54bdf8cde87907f52b1a89bdcd 246B / 246B 
 => => sha256:c88f260614fd547590965c08dd4dfd04abf2a6b974e16d7399974deb1191ff09 895B / 895B  
 => => extracting sha256:ee0a4e40ccac1c58f6ca3cf6c0cbb0a5897abd33dee49b641509d5627df02b46 
 => => extracting sha256:5ca9fb408faadb7bf25d58bd66104b2e93e7ba37e961bb24b245abcc09230187 
 => => extracting sha256:25862b797d8786d6655c181772787e4fa6c932aafd6853e429409590e9f6d99b  
 => => extracting sha256:5c128100b25a1cf17b571b5c0c8d316f25c03b14b0315f97302a72713953847a     
 => => extracting sha256:c471030ba37f74280181f8e9174e2e45192728be49c4023d12ea165263852174 
 => => extracting sha256:285a5ec09eb5e2d43e7ad5c490ed118f537ed46d9be12e573a5fa8aa24632f51  
 => => extracting sha256:aae08d904de274c3439581ae207ac32759146fce216b6744dd1e9dad7bff40e7  
 => => extracting sha256:b25acd918410cd04cf6d18be60764dd927d398f92f46a57d91e2f5ab7b022002 
 => => extracting sha256:3e0ec17247cba6f489a3591694cd88fffa3dcc7787f34c0164dec799980edb07   
 => => extracting sha256:53d5f7669583566f165bfceb2ed3e0b8435b7c32b27b9d7c8bf847126ccba9d0   
 => => extracting sha256:d778c6e5f84cb3078c07f5fac6b8580c70a60a54bdf8cde87907f52b1a89bdcd  
 => => extracting sha256:c88f260614fd547590965c08dd4dfd04abf2a6b974e16d7399974deb1191ff09  
 => [2/2] COPY . /var/www/html/ 
 => exporting to image  
 => => exporting layers 
 => => writing image sha256:a433393f1ac2d28f83b4848d4adf2618d3f6809964a33072cef0f1d09bc02b63 
 => => naming to docker.io/milissenrobin/multi-cloud-app:v1
```

NOTE: the trailing dot ('.') of the command is very important, do not omit it when copy/pasting

You will now have 1 docker image on your system:

```
docker image ls

---

REPOSITORY                      TAG       IMAGE ID       CREATED         SIZE
milissenrobin/multi-cloud-app   v1        a433393f1ac2   6 minutes ago   460MB
```

## 5. Test your Docker image ##

To start a container from the Docker image you created above, run the following command (make sure to replace `YOURDOCKERHUBACCOUNTNAME` with your actual Docker Hub account name):

```
docker container run -d -p 80:80 YOURDOCKERHUBACCOUNTNAME/multi-cloud-app:v1
```

Open your browser again and visit http://X.mgmt.workshop.aws.gluo.cloud to see if your application is running.  If you do not see the multi-cloud-app website let one of the instructors know!

## 6. Push the image to the Docker Hub ##

Next you will push your Docker image to your personal Docker Hub account, but first you'll need to log into your account from the management server. Use the command below:

```
docker login

---

Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: milissenrobin
Password: 
WARNING! Your password will be stored unencrypted in /home/ubuntu/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

Now you are ready to push your Docker image to the Docker Hub, to do so use the command below:

```
docker image push YOURDOCKERHUBACCOUNTNAME/multi-cloud-app:v1

---

The push refers to repository [docker.io/milissenrobin/multi-cloud-app]
59e1c7035805: Pushed 
3d4673133c73: Pushed 
fcf727d04a07: Pushed 
2dcb272edb2e: Pushed 
07e4eaa277f8: Pushed 
d6541cb9f660: Pushed 
99b3be0fcf3d: Pushed 
4adc4f8ad1cb: Pushed 
07583c9c3016: Pushed 
8b80409103ee: Pushed 
cfb7053350c4: Pushed 
c4bd73e3313b: Pushed 
b45c80a24442: Pushed 
3af14c9a24c9: Pushed 
v1: digest: sha256:d100261f16871db7284af3c341b424bb4682eb66781f47b2710e8a7d66e4cf24 size: 3245
```

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM

Run the checkscore command.
- `sudo checkscore 1`

## 7. (Optional) Test an image from your colleagues ##

Post the `docker container run` command you used to Slack, for example: `docker container run -d -p 80:80 YOURDOCKERHUBACCOUNTNAME/multi-cloud-app:v1`

Now stop and remove you own container:

```
docker container rm -f $(docker container ls -ql)
```

Now run one of the `docker container run` commands that one of colleagues posted to Slack, visit http://X.mgmt.workshop.aws.gluo.cloud again to see that also your colleagues image works without any issues on your server.

This is a good example of how easy Docker makes it to move application from one server to another (this is exactly why we will be using Docker in the next labs).

---

Go to the next lab: [Lab 02 - Manual setup](../lab02_manual_setup)
