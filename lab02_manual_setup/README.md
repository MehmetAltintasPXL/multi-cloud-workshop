# Lab 02 - Manual Setup

In this lab we will do everyting manually:

1. we will create a VM
1. we will create a DNS record (CNAME) which we will point to the public IP of the VM
1. we will run a little script that will first install Docker and afterwards will start the nginx container
1. we will verify that everything works by visiting the nginx application in our browser

NOTE: please follow the instruction on the screen carefully, should you run behind let one of the instructors know!
IMPORTANT: Before starting you'll need to make sure you have selected the correct aws region. In the top right of the screen make sure that it says `Ireland`, if not select the `Europe (Ireland) eu-west-1`.
![](../images/CloudformationRegion.png)

## 1. AWS - EC2
---
Go to the AWS console 'https://gluo-workshop.signin.aws.amazon.com/console', type in the top searchbar EC2 and open the EC2 page.
![](../images/ec2Service.png)

* Create a VM: t2.micro
    1. Click on launch instances.
      ![](../images/launchEc2Instance.png)

    1. Give the instance a name (***X-manual***). `Replace X with your group number`. 
      ![](../images/ec2Name.png)

    1. Click on the **Add additional tags** link (right of the textbox). Next, click on the **Add Tag** button and fill in **group** as the key, and **your group number** as the value. Repeat the process by clicking the Add Tag button again, and this time fill in **lab** as the key, and **2** as the value. `Replace X with your group number`
    
       ![](../images/ec2Tags.png)
    
    1. Select Ubuntu as OS image.
      ![](../images/ec2Image.png)
    
    1. Leave the instance type on `t2.micro`.
    
    1. Select the **workshop-key-students** Key pair.
      ![](../images/ec2KeyPair.png)
    
    1. Under Network settings select `Select existing security group`, click on the dropdown under common security groups and select the security group `manual-setup-security-group`.
      ![](../images/ec2NetworkSettings.png)

    1. Click on `Launch instance` in the bottom right.
    
    1. Then click on View all instances.
    
    1. Select your instance and on the bottom right of the screen, copy the `Public IPv4 DNS` adress to your clipboard.

![](../images/ec2PublicDNS.png)

---
&nbsp;  
## 2. Route53
Go to the AWS console 'https://gluo-workshop.signin.aws.amazon.com/console', type in the top searchbar route53 and open the route53 page.  
**Note:** Ignore the error message appearing on the Route 53 page since these are merely related to limited access rights for this workshop. The purpose for which Route53 is needed in this lab, which is creating a DNS-record for the manual created VM, is still possible. 
![](../images/route53Service.png)

* Create a DNS record: X-manual.workshop.aws.gluo.cloud
    1. On the Route 53 Dashboard click on `Hosted zones` link.
    ![](../images/route53HostedZones.png)

    1. Click on gluo.cloud and then on Create record.
    ![](../images/route53HostedZone.png)

    1. On the next page you can create a new record. As the **Record name** type X-manual (E.g. 5-manual). Change the **Record type** from `A` to `CNAME`. Paste the `Public IPv4 DNS` from your EC2 instance you copied before in the `value textbox`. Change the `TTL` to `60`.
    ![](../images/route53CreateRecord.png)

* SSH into your newly created EC2 instance from your management server.  
**Note:** During the manual creation of this EC2, you assigned the workshop-key-students keypair to log into this VM. The private key, which is used for the SSH-connection, is already present in the .ssh folder on your management server. 

    ``` 
    ssh -i ~/.ssh/workshop_key.pem ubuntu@X-manual.workshop.aws.gluo.cloud
    ```

* Use the following command to run a script located on the Gitlab multi-cloud-workshop repository. This script will first install Docker, then add the ubuntu user to the Docker group. Afterwards it will start an nginx Docker container.

    ```
    curl -fsSL https://gitlab.com/gluo-workshop/multi-cloud-workshop/-/raw/main/lab02_manual_setup/setup | sh
    ```

* Visit webpage: http://X-manual.workshop.aws.gluo.cloud
&nbsp;  
## 3. Conclusion
In this lab you have manually created an EC2 VM and assigned a DNS-record to it. You connected to this VM through SSH, installed Docker and initiated an default nginx container by using a script.
Upon visiting the webpage http://X-manual.workshop.aws.gluo.cloud you saw your DNS-record successfully redirecting to the EC2's public IPv4 address, where the nginx container is up and running. 

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM
Run the checkscore command.
- `sudo checkscore 2`

---

Go to the next lab: [Lab 03 - Infrastructure 2.0](../lab03_infra2.0)
