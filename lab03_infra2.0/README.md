# Lab 03 - Infrastructure 2.0 #

## 1. Fork the workshop repository ##

Browse to the workshop GitLab page:

- URL: https://gitlab.com/gluo-workshop/multi-cloud-workshop 

On the top right you should see a `Fork` button, click this button and fork the repository into your previously assigned GitLab group.

![](../images/forkRepo.png)

## 2. Clone repository ##

Before proceeding you will need to clone the repository that contains the Cloudformation template and Bicep code.  
Run the following command from the management server:

```
cd ~
git clone git@gitlab.com:gluo-workshops/multicloud-workshop/YOUR_GROUP_NAME/multi-cloud-workshop.git

---

Cloning into 'multi-cloud-workshop'...
remote: Enumerating objects: 442, done.
remote: Counting objects: 100% (295/295), done.
remote: Compressing objects: 100% (127/127), done.
remote: Total 442 (delta 146), reused 295 (delta 146), pack-reused 147
Receiving objects: 100% (442/442), 2.47 MiB | 5.49 MiB/s, done.
Resolving deltas: 100% (192/192), done.
```

***Go into the cloned repository lab03_infra2.0 directory:***

```
cd ~/multi-cloud-workshop/lab03_infra2.0
```

## 3. CloudFormation on AWS ##

CloudFormation, Bicep and Terraform are all **Infrastructure-as-code** (IaC) tools:

- CloudFormation is developed by Amazon and only manages AWS resources. 
- Bicep is created by Microsoft and only manages Azure resources.
- Terraform is developed by HashiCorp and can manage resources across a wide range of cloud vendors.

&nbsp;  

In this lab we will use CloudFormation to deploy our EC2 instance and Route53 records to AWS.  


The file `cloudformation.yml` is already present in the repo directory **but you will also have to copy this file to your  local machine.** You will need to upload this file to the AWS Console later.

We can do this using ***scp***.
You'll have to replace the `X` in the following command with your group number.
***On your local host (Not on the remote vm) run the following command:***

```
scp -i workshop_key.pem ubuntu@X.mgmt.workshop.aws.gluo.cloud:~/multi-cloud-workshop/lab03_infra2.0/cloudformation.yml .
```

```yaml
cat cloudformation.yml

...
AWSTemplateFormatVersion: '2010-09-09'
Description: 'AWS CloudFormation Sample Template EC2InstanceWithSecurityGroupSample:
  Create an Amazon EC2 instance running the Amazon Linux AMI. The AMI is chosen based
  on the region in which the stack is run. This example creates an EC2 security group
  for the instance to give you SSH access. **WARNING** This template creates an Amazon
  EC2 instance. You will be billed for the AWS resources used if you create a stack
  from this template.'
Parameters:
  KeyName:
    Description: Name of an existing EC2 KeyPair to enable SSH access to the instance
    Type: AWS::EC2::KeyPair::KeyName
    ConstraintDescription: must be the name of an existing EC2 KeyPair.
  InstanceType:
    Description: WebServer EC2 instance type
    Type: String
    Default: t2.micro
    AllowedValues:
    - t2.micro
    ConstraintDescription: must be a valid EC2 instance type.
...
```

&nbsp;  

Next, perform the following steps to open the CloudFormation dashboard and create a new stack:

1. Select `Services` on the top left of the Console screen.

1. Click on `CloudFormation` (or search for it first and then click on the link)

1. Before creating a CloudFormation stack you'll need to make sure you have selected the correct aws region. In the top right of the screen make sure that it says `Ireland`, if not select the `Europe (Ireland) eu-west-1`.
  ![](../images/CloudformationRegion.png)

1. Click on `Create Stack` on the right side of the Console screen and select `With new resources(standard)`

   ![](../images/cloudFormationCreateStack.png)

1. Select the `Template is ready` option, then under ***Specify template*** select `Upload a template file`.

   ![](../images/cloudFormationUploadTemplate.png)

1. Click the `Choose file` button

1. Search for the `cloudformation.yml` file you created above and select it

   ![](../images/cloudFormationChooseFile.png)

1. Click the orange `Next` button in the bottom right

1. Enter the `Stack name`, this should be **groupX** (replace the X with your workshop group ID e.g. group1, group2, ...)

1. Enter the `GroupID`. Replace the X with your workshop group ID. 

1. Ignore the `InstanceType` setting, since the current default **t2.micro** is the only one allowed for this workshop.

1. In the `KeyName` dropdown select the **workshop-key-students** EC2 keypair. 

![image-20230105093936804](../images/cloudFormationStackSpecs.png)

12. Click the orange `Next` button.

13. You can ***ignore the red warnings***, simply scroll to the bottom of the page and click the orange `Next` button.

14. Scroll yet again to the bottom of the page and click the orange `Submit` button.

You should now see that your stack is being created.  
**Note:** If you want to follow the progress of your stack creation, select your stackname and open the Events tab. There is a refresh button which you can use to track the current status.   

&nbsp;  

When the creation is completed, go to the `EC2` and `Route53` section of the AWS Console to verify that your resources have actually been created.

Wait a few minutes for the VM to be ready.
Go to http://X.cloudformation.workshop.aws.gluo.cloud/ to see if the nginx webpage is online, if not, try again later.

## 4. Bicep Templates on Azure ##
Next, we will use Azure Bicep templates to deploy the infastructure on Azure.
Sign in to the Microsoft account with Azure CLI using the service principal:

SSH into the management VM and run the following command to sign in to the Microsoft account.  
**Note:** The variables used in this command are already stored as environment variables on the management VM.  
```bash
az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
```

The main.bicep template is shown below:
```bicep
param groupID string
param location string = 'westeurope'
param vnetRGName string = 'multicloud-workshop'
param vnetName string = 'multicloud-workshop-vnet'
param subnetName string = 'default'
param adminUsername string = 'ubuntu'


resource virtualMachine 'Microsoft.Compute/virtualMachines@2022-03-01' = {
  name: 'bicep-instance-${groupID}'
  location: location
  tags: {
    group: groupID
    lab: '3'
  }
  properties: {
    networkProfile: {
      networkInterfaces: [
        {
          id: nic.id
        }
      ]
    }
    hardwareProfile: {
      vmSize: 'Standard_B1ms'
    }
    storageProfile: {
      osDisk: {
        name: 'bicep-OsDisk-${groupID}'
        caching: 'ReadWrite'
        createOption: 'FromImage'
        managedDisk: {
          storageAccountType: 'Premium_LRS'
        }
        deleteOption: 'Delete'
      }
      imageReference: {
        publisher: 'canonical'
        offer: '0001-com-ubuntu-server-focal'
        sku: '20_04-lts-gen2'
        version: 'latest'
      }
    }
    osProfile: {
      computerName: 'bicep-instance-${groupID}'
      adminUsername: adminUsername
      customData: loadFileAsBase64('cloud-init.yaml')
      linuxConfiguration: {
        disablePasswordAuthentication: true
        ssh: {
          publicKeys: [
            {
              path: '/home/ubuntu/.ssh/authorized_keys'
              keyData: loadTextContent('./workshop_key.pub')
            }
          ]
        }
      }
    }
  }
}

resource publicIp 'Microsoft.Network/publicIPAddresses@2022-05-01' = {
  name: 'bicep-publicip-${groupID}'
  location: location
  properties: {
    publicIPAllocationMethod: 'Static'
  }
}

resource securityGroup 'Microsoft.Network/networkSecurityGroups@2022-05-01' = {
  name: 'bicep-sg-${groupID}'
  location: location
  properties: {
    securityRules: [
      {
        name: 'SSH'
        properties: {
          priority: 1001
          direction: 'Inbound'
          access: 'Allow'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '22'
          sourceAddressPrefix: '*'
          destinationAddressPrefix: '*'
        }
      }
      {
        name: 'HTTP'
        properties: {
          priority: 1011
          direction: 'Inbound'
          access: 'Allow'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '80'
          sourceAddressPrefix: '*'
          destinationAddressPrefix: '*'
        }
      }
    ]
  }
}

resource subnet 'Microsoft.Network/virtualNetworks/subnets@2022-05-01' existing = {
  name: '${vnetName}/${subnetName}'
  scope: resourceGroup(vnetRGName)
}

resource nic 'Microsoft.Network/networkInterfaces@2022-05-01' = {
  name: 'bicep-nic-${groupID}'
  location: location
  properties: {
    ipConfigurations: [
      {
        name: 'bicep-nicCfg-${groupID}'
        properties: {
          subnet: {
            id: subnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
          publicIPAddress: {
            id: publicIp.id
          }
        }
      }
    ]
  }
}

module dns 'dns.bicep' = {
  name: 'bicep-dns-${groupID}'
  scope: resourceGroup('rg-gluosandbox-core')
  params: {
    groupID: groupID
    dnsZoneName: 'workshop.azure.gluo.cloud'
    instanceIpAddress: publicIp.properties.ipAddress
  }
}
```

The dns.bicep module is shown below:
```bicep
param groupID string
param dnsZoneName string
param instanceIpAddress string

resource zone 'Microsoft.Network/dnsZones@2018-05-01' existing = {
  name: dnsZoneName
}

resource record 'Microsoft.Network/dnsZones/A@2018-05-01' = {
  parent: zone
  name: '${groupID}.bicep'
  properties: {
    ARecords: [
      {
        ipv4Address: instanceIpAddress
      }
    ]
    TTL: 60
  }
}
```

Copy the workshop_key.pub to the right directory.

```
cp ~/.ssh/workshop_key.pub ~/multi-cloud-workshop/lab03_infra2.0
```

Change the 2 **X**'s in the following command and run it. This will create a new deployment in the target resource group and deploy all the requested resources on Azure.

```
cd ~/multi-cloud-workshop/lab03_infra2.0
az deployment group create --name groupX --resource-group multicloud-workshop-bicep --template-file main.bicep --parameters groupID=X
```

Wait a few minutes for the VM to be ready.
Go to http://X.bicep.workshop.azure.gluo.cloud/ to see if the nginx webpage is online, if not, try again later.

## RUN CHECKSCORE SCRIPT ON YOUR MANAGEMENT VM

After all resources are successfully set up, run the checkscore command.
- `sudo checkscore 3`

## 5. Cleanup ##

### AWS ###

Go back to the `CloudFormation` section, select your own stack and click the `Delete` button, confirm by clicking the orange `Delete stack` button. As soon as your stack has been successfully deleted, visit the `EC2` and `Route53` sections again to confirm that your resources have indeed been cleaned up completely.

## Azure ##

**IMPORTANT!**   
The instructor will cleanup the infrastructure deployed with the Azure Bicep template.

As of this writing, Azure Bicep has **NO** fuction to remove the infrastructure apart from removing the entire resource group.


## 6. Conclusion

In this lab you have created two infrastructure stacks, one on AWS and one on Azure, by using a CloudFormation (AWS) and Bicep (Azure) template. This shows that many of the manual steps you did in the previous lab can be automated by using Infrastructure-as-Code tools.

---

Go to the next lab: [Lab 04 - Terraform](../lab04_terraform)