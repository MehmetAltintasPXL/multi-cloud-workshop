param groupID string
param location string = 'westeurope'
param vnetRGName string = 'multicloud-workshop'
param vnetName string = 'multicloud-workshop-vnet'
param subnetName string = 'default'
param adminUsername string = 'ubuntu'


resource virtualMachine 'Microsoft.Compute/virtualMachines@2022-03-01' = {
  name: 'bicep-instance-${groupID}'
  location: location
  tags: {
    group: groupID
    lab: '3'
  }
  properties: {
    networkProfile: {
      networkInterfaces: [
        {
          id: nic.id
        }
      ]
    }
    hardwareProfile: {
      vmSize: 'Standard_B1ms'
    }
    storageProfile: {
      osDisk: {
        name: 'bicep-OsDisk-${groupID}'
        caching: 'ReadWrite'
        createOption: 'FromImage'
        managedDisk: {
          storageAccountType: 'Premium_LRS'
        }
        deleteOption: 'Delete'
      }
      imageReference: {
        publisher: 'canonical'
        offer: '0001-com-ubuntu-server-focal'
        sku: '20_04-lts-gen2'
        version: 'latest'
      }
    }
    osProfile: {
      computerName: 'bicep-instance-${groupID}'
      adminUsername: adminUsername
      customData: loadFileAsBase64('cloud-init.yaml')
      linuxConfiguration: {
        disablePasswordAuthentication: true
        ssh: {
          publicKeys: [
            {
              path: '/home/ubuntu/.ssh/authorized_keys'
              keyData: loadTextContent('./workshop_key.pub')
            }
          ]
        }
      }
    }
  }
}

resource publicIp 'Microsoft.Network/publicIPAddresses@2022-05-01' = {
  name: 'bicep-publicip-${groupID}'
  location: location
  properties: {
    publicIPAllocationMethod: 'Static'
  }
}

resource securityGroup 'Microsoft.Network/networkSecurityGroups@2022-05-01' = {
  name: 'bicep-sg-${groupID}'
  location: location
  properties: {
    securityRules: [
      {
        name: 'SSH'
        properties: {
          priority: 1001
          direction: 'Inbound'
          access: 'Allow'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '22'
          sourceAddressPrefix: '*'
          destinationAddressPrefix: '*'
        }
      }
      {
        name: 'HTTP'
        properties: {
          priority: 1011
          direction: 'Inbound'
          access: 'Allow'
          protocol: 'Tcp'
          sourcePortRange: '*'
          destinationPortRange: '80'
          sourceAddressPrefix: '*'
          destinationAddressPrefix: '*'
        }
      }
    ]
  }
}

resource subnet 'Microsoft.Network/virtualNetworks/subnets@2022-05-01' existing = {
  name: '${vnetName}/${subnetName}'
  scope: resourceGroup(vnetRGName)
}

resource nic 'Microsoft.Network/networkInterfaces@2022-05-01' = {
  name: 'bicep-nic-${groupID}'
  location: location
  properties: {
    ipConfigurations: [
      {
        name: 'bicep-nicCfg-${groupID}'
        properties: {
          subnet: {
            id: subnet.id
          }
          privateIPAllocationMethod: 'Dynamic'
          publicIPAddress: {
            id: publicIp.id
          }
        }
      }
    ]
  }
}

module dns 'dns.bicep' = {
  name: 'bicep-dns-${groupID}'
  scope: resourceGroup('rg-gluosandbox-core')
  params: {
    groupID: groupID
    dnsZoneName: 'workshop.azure.gluo.cloud'
    instanceIpAddress: publicIp.properties.ipAddress
  }
}
