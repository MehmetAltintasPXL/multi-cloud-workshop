param groupID string
param dnsZoneName string
param instanceIpAddress string

resource zone 'Microsoft.Network/dnsZones@2018-05-01' existing = {
  name: dnsZoneName
}

resource record 'Microsoft.Network/dnsZones/A@2018-05-01' = {
  parent: zone
  name: '${groupID}.bicep'
  properties: {
    ARecords: [
      {
        ipv4Address: instanceIpAddress
      }
    ]
    TTL: 60
  }
}
