provider "aws" {
}

terraform {
  backend "http" { // <==== Change this to "http" to run in the GitLab pipeline
  }
}

resource "aws_instance" "ec2-instance" {
  ami             = "${var.aws_ami}"
  instance_type   = "${var.aws_machine_type}"
  key_name        = "${aws_key_pair.workshop_key.key_name}"
  
  security_groups = [
    "${aws_security_group.allow_ssh.name}",
    "${aws_security_group.allow_http.name}",
    "${aws_security_group.allow_outbound.name}"
  ]

  user_data       = templatefile("cloud-init.yaml",{group_id = "${var.groupID}"})

  tags = {
    Name = "terraform-instance-aws-group${var.groupID}"
    group = "${var.groupID}"
    lab = "${var.lab}"
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh-group${var.groupID}"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http" {
 name        = "allow-http-group${var.groupID}"
 description = "Allow HTTP inbound traffic"
 ingress {
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
}

resource "aws_security_group" "allow_outbound" {
  name        = "allow-all-outbound-group${var.groupID}"
  description = "Allow all outbound traffic"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "workshop_key" {
  key_name   = "workshop-key-group${var.groupID}"
  public_key = "${file("~/.ssh/workshop_key.pub")}"
}

resource "aws_route53_record" "gluo-cloud" {
  zone_id = var.dns_zone_id
  name    = "${var.groupID}.tf.workshop.aws.gluo.cloud."
  type    = "CNAME"
  ttl     = "60"
  records = ["${aws_instance.ec2-instance.public_dns}"]
}
